CHAT APP - INSTRUCTIONS

1 . clone the repository using the command 
git clone https://gitlab.com/pranay8297/chat_app

2. After cloning the repo, need to do 2 things first
    i . create a python server
    ii . create a sqlite3 database and link it through sandman2

3 . for creating a server run the following command
    python -m http.server 8000

4 . for creating a database and setting up tables, run the following commands in 
    the terminal
    
    sqlite3 database_name
        CREATE TABLE id_names(id int primary key , name);
        CREATE TABLE names (id integer primary key autoincrement , name text);
        CREATE TABLE sqlite_sequence(name,seq);
        CREATE TABLE channels (channelname text primary key);
        CREATE TABLE messages(mid integer primary key autoincrement , uid integer , uname text , cname text , messagebody text , FOREIGN KEY(cname) REFERENCES channels(channelname) , FOREIGN KEY(uid) REFERENCES names(id) , FOREIGN KEY (uname) REFERENCES names(name));
    
    now that our schema is ready, you are boom roasted, go ahed and follow one last step
    
    Then run the following command in the terminal to start the server
        sandman2ctl sqlite+pysqlite:///database_name

5 . Go to the browser and open the server on specified port, and also open the database server under admin provision to look at the data flow.
    i . A login page is displayed, provide a username to continue   
    ii . After logging in, a page consisting list of channels and messages corrosponding to it are displayed
    iii . Click on a channel you want to chat in, and chat....
