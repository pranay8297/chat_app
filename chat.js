function loginSuccess(response){
    //console.log(resp)
    window.location.replace('http://localhost:8000/channels.html');  
}

function loginAjaxPost(dataObject , url , successFunction){
    $.ajax({
        type : 'POST',
        crossDomain: 'true',
        url : url,
        dataType : 'json',
        data : JSON.stringify(dataObject),
        success : successFunction()
    })
}

function findUserId(userName){
    $.get("http://localhost:5000/names?name="+ userName , function(response){
            return response.resources['id']
        })
}

function displayListOfChannels(userName){
    $.get("http://localhost:5000/channels", function(response) {
        for (let i of response.resources) {
            $("<button/>").attr('id',i["channelname"]).text(i["channelname"]).appendTo('#list-of-channels');
        }
        $('.user-name').text(userName)
        localStorage.channelname = response.resources[0].channelname
    })
}

function createChannelPost(url , dataObject ,  channelName){
    $('#channel-name').removeClass('channel-already-present');
    $.ajax({
        crossDomain: 'true',
        type: "POST",
        url: url,
        data: JSON.stringify(dataObject),
        dataType: "json",
        success: function (response) {
                     if(response != undefined){
                         $("<button/>").attr('id',channelName).text(channelName).appendTo('#list-of-channels');
                     }
                     else{
                         $('#channel-name').addClass('channel-already-present');
                     }
                 }
    })
}

///////////present/////////////////////////////////
function displayListOfMessages(cname){
    // $('#'+cname).css('background-color','rgb(13, 13, 61)')
    $.get('http://localhost:5000/messages?cname='+ cname, function(response){
            for(let i of response.resources){
                $('#message-list').append('<p>' + i['messagebody'] + ' - '+ i['uname'])
            }
        })
}

//////////////present//////////////////////////////
function messageBroadcast(tableData , response){
    $.ajax({
        crossDomain : 'true',
        type : 'POST',
        url : 'http://localhost:5000/broadcast',
        dataType: 'json',
        contentType:'json',
        data : JSON.stringify(tableData),
        success : function(response){
            console.log('message broadcasted successfully')
        }
    })
}

function sendMessage(tableData){
    $.ajax({
        crossDomain : 'true',
        type : 'POST',
        url : 'http://localhost:5000/messages/',
        dataType: 'json',
        data : JSON.stringify(tableData),
        success : messageBroadcast(tableData)
        })
}

$(document).ready(function() {
    $('#input_button').click(function(event){
        event.preventDefault()
        let text = $('#input_name').val();
        localStorage.userName = text
        let t = {'name' : text}
        let url = 'http://localhost:5000/names'
        loginAjaxPost(t , url , loginSuccess)


        $.get("http://localhost:5000/names?name="+ localStorage.userName , function(response){
             localStorage.userId = response.resources['id']
        })        
        console.log(localStorage.userName)
        console.log(localStorage.userId)
    })

    const user = localStorage.userName
    const user_id = localStorage.userId


    //////////////// pusher//////////////
    Pusher.logToConsole = true;
    
    console.log(user)
    console.log(user_id)
    var pusher = new Pusher('6b4deba4b626b196fc0e', {
      cluster: 'ap2',
      forceTLS: true
    });
    
    var channel = pusher.subscribe('messages');
    channel.bind('message-added', function(data) {
      $('#message-list').append('<p>' + data.messagebody + ' - ' +  data.uname)
      $('#message-body').val('').focus();
    });
    ///////////////pusher/////////////////
    displayListOfChannels(user)

    /////////////////// channel //// lsit /// complete
    $('#channel-create').click(function (e) { 
        e.preventDefault();
        $('#creation').toggle();
    })
    
    $('#create-channel-button').click(function(e){
        e.preventDefault()
        let channelName = $('#channel-name').val()
        let chname = {'channelname' : channelName}
        let url = "http://localhost:5000/channels/"
        createChannelPost(url , chname , channelName)
    })
    
    ///////present//////////////////////////////
    $('#list-of-channels').click(function(e){
        e.preventDefault()

        $('<button>').removeClass('channel-selected')
        let ele = $(e.target).attr('id')
        localStorage.channelname = ele
        // $('#'+$ele).css('background-color','rgb(13, 13, 61)')     
        $('#message-list').empty();
        $(ele).addClass('channel-selected');
        displayListOfMessages(ele)
    })
    ////////present////////////////


    $('#message-send-button').click(function(e){
        e.preventDefault()
        let msg = $('#message-body').val()
        tableData = {
            'uid' : user_id,
            'cname' : localStorage.channelname,
            'messagebody' : msg,
            'uname' : user
        }
        sendMessage(tableData)
    })
})
    
